package controllers

import javax.inject._
import play.api._
import play.api.mvc._

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
}

class PostController @Inject()(cc: MessagesControllerComponents) extends MessagesAbstractController(cc) {
  import CodeForm._
  // The action that handles our form post
  def debugCode = Action { implicit request: MessagesRequest[AnyContent] =>

    val successFunction = { data: Data =>
      Logger.info("name: \"" + data.name + "\"")
      Ok("{result: \"ok\", name: \"" + data.name + "\"}")
    }

    val formValidationResult = form.bindFromRequest
    formValidationResult.fold(formWithErrors => BadRequest("bad"), successFunction)

  }
}
