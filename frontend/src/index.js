import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import $ from 'jquery';
import confirm from 'jquery';

const reactFormContainer = document.querySelector('.react-form-container')

class ReactFormLabel extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <label htmlFor={this.props.htmlFor}>{this.props.title}</label>
    )
  }
}

class ReactForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      name1: 'Name',
      value1: 'Value',
      name2: 'Name',
      value2: 'Value',      
      height: '',
      LastBlockUtxoRootHash: '',
      code: ''
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange = (e) => {
    let newState = {}

    newState[e.target.name] = e.target.value

    this.setState(newState)
  }


  handleSubmit = (e, message) => {
    e.preventDefault()

    let formData = {
      formName1: this.state.name1,
      formValue1: this.state.value1,
      formName2: this.state.name2,
      formValue2: this.state.value2,
      formHeight: this.state.height,
      formLastBlockUtxoRootHash: this.state.LastBlockUtxoRootHash,
      formCode: this.state.code
    }

    if (formData.formCode.length < 1) {
      return false
    }

    $.ajax({
      url: 'http://localhost:8000/',
      dataType: 'json',
      type: 'POST',
      data: formData,
      success: function (data) {
        if (confirm('Thank you for your message. Can I erase the form?')) {
          this.setState({
            firstName: '',
            lastName: '',
            email: '',
            subject: '',
            message: ''
          })
        }
      },
      error: function (xhr, status, err) {
        console.error(status, err.toString())
        alert('There was some problem with sending your message.')
      }
    })

    this.setState({
      firstName: '',
      lastName: '',
      email: '',
      subject: '',
      message: ''
    })
  }

  render() {
    return (
      <form className='react-form' onSubmit={this.handleSubmit}>
        <h1>Set your parameters</h1>

        <fieldset className='form-group'>
          <ReactFormLabel htmlFor='formName1' title='Parameter 1 :' />

          <input id='formName1' className='form-input' name='name1' type='text' required onChange={this.handleChange} value={this.state.name1} />
          <input id='formName1' className='form-input' name='value1' type='text' required onChange={this.handleChange} value={this.state.value1} />

        </fieldset>

        <fieldset className='form-group'>
          <ReactFormLabel htmlFor='formName2' title='Parameter 2 :' />

          <input id='formName2' className='form-input' name='name2' type='text' required onChange={this.handleChange} value={this.state.name2} />
          <input id='formName2' className='form-input' name='value2' type='text' required onChange={this.handleChange} value={this.state.value2} />

        </fieldset>

        <fieldset className='form-group'>
          <ReactFormLabel htmlFor='formHeight' title='Height :' />

          <input id='formHeight' className='form-input' name='height' type='text' required onChange={this.handleChange} value={this.state.height} />
        </fieldset>

        <fieldset className='form-group'>
          <ReactFormLabel htmlFor='formLastBlockUtxoRootHash' title='Last Block Utxo Root Hash :' />

          <input id='formLastBlockUtxoRootHash' className='form-input' name='LastBlockUtxoRootHash' type='text' required onChange={this.handleChange} value={this.state.LastBlockUtxoRootHash} />
        </fieldset>

        <fieldset className='form-group'>
          <ReactFormLabel htmlFor='formCode' title='Code:' />

          <textarea id='formCode' className='form-textarea' name='code' required onChange={this.handleChange} value={this.state.code}></textarea>
        </fieldset>

        <div className='form-group'>
          <input id='formButton' className='btn' type='submit' placeholder='Send message' />
        </div>
      </form>
    )
  }
}

ReactDOM.render(<ReactForm />, reactFormContainer)
